package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.spring5webapp.model.IncidenRegistry;

public interface IncidenRegistryRepository extends CrudRepository<IncidenRegistry, Long> {
}
