package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.spring5webapp.model.Inventory;

public interface InventoryRepository extends CrudRepository<Inventory, Long> {
}
