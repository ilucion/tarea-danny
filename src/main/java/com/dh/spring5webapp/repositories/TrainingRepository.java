package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.spring5webapp.model.Training;

public interface TrainingRepository extends CrudRepository<Training, Long> {
}
