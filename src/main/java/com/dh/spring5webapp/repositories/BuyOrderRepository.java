package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.spring5webapp.model.BuyOrder;

public interface BuyOrderRepository extends CrudRepository<BuyOrder, Long> {
}
