package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.spring5webapp.model.Incident;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
}
