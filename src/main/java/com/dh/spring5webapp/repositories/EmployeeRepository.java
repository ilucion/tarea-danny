/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
