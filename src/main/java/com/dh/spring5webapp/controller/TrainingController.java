package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.services.TrainingService;

@Controller
public class TrainingController {
    private TrainingService trainingsService;

    public TrainingController(TrainingService trainingsService) {
        this.trainingsService = trainingsService;
    }

    @RequestMapping("/trainings")
    public String getTraining(Model model) {
        model.addAttribute("trainings", trainingsService.getTrainings());
        return "trainings";
    }
}