package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.services.BuyOrderService;

@Controller
public class BuyOrderController {
    private BuyOrderService buyOrderService;

    public BuyOrderController(BuyOrderService buyOrderService) {
        this.buyOrderService = buyOrderService;
    }

    @RequestMapping("/buyOrders")
    public String getBuyOrders(Model model) {
        model.addAttribute("buyOrders", buyOrderService.getBuyOrders());
        return "buyOrders";
    }
}