package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Inventory;

import java.util.Set;

public interface InventoryService {
    Set<Inventory> getInventories();
}