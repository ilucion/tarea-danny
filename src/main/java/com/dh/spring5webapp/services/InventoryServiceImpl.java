package com.dh.spring5webapp.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dh.spring5webapp.model.Inventory;
import com.dh.spring5webapp.repositories.InventoryRepository;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    public Set<Inventory> getInventories() {

        Set<Inventory> inventoriess = new HashSet<>();

        inventoryRepository.findAll().iterator().forEachRemaining(inventoriess::add);
        return inventoriess;
    }

}