package com.dh.spring5webapp.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dh.spring5webapp.model.Incident;
import com.dh.spring5webapp.repositories.IncidentRepository;

@Service
public class IncidentServiceImpl implements IncidentService {

    @Autowired
    private IncidentRepository incidentRepository;

    @Override
    public Set<Incident> getIncidents() {

        Set<Incident> incidents = new HashSet<>();

        incidentRepository.findAll().iterator().forEachRemaining(incidents::add);
        return incidents;
    }

}