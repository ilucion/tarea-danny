package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.Incident;

import java.util.Set;

public interface IncidentService {
    Set<Incident> getIncidents();
}