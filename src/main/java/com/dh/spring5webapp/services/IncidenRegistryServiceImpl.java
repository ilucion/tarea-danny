package com.dh.spring5webapp.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dh.spring5webapp.model.IncidenRegistry;
import com.dh.spring5webapp.repositories.IncidenRegistryRepository;

@Service
public class IncidenRegistryServiceImpl implements IncidenRegistryService {

    @Autowired
    private IncidenRegistryRepository incidenRegistryRepository;

    @Override
    public Set<IncidenRegistry> getIncidenRegistries() {

        Set<IncidenRegistry> IncidenRegistriess = new HashSet<>();

        incidenRegistryRepository.findAll().iterator().forEachRemaining(IncidenRegistriess::add);
        return IncidenRegistriess;
    }

}