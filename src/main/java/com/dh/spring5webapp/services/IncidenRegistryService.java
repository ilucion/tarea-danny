package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.IncidenRegistry;

import java.util.Set;

public interface IncidenRegistryService {
    Set<IncidenRegistry> getIncidenRegistries();
}