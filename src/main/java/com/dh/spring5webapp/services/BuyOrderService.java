package com.dh.spring5webapp.services;

import com.dh.spring5webapp.model.BuyOrder;

import java.util.Set;

public interface BuyOrderService {
    Set<BuyOrder> getBuyOrders();
}