package com.dh.spring5webapp.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dh.spring5webapp.model.BuyOrder;
import com.dh.spring5webapp.repositories.BuyOrderRepository;

@Service
public class BuyOrderServiceImpl implements BuyOrderService {

    @Autowired
    private BuyOrderRepository buyOrderRepository;

    @Override
    public Set<BuyOrder> getBuyOrders() {

        Set<BuyOrder> buyOrders = new HashSet<>();

        buyOrderRepository.findAll().iterator().forEachRemaining(buyOrders::add);
        return buyOrders;
    }

}