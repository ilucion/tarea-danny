package com.dh.spring5webapp.model;

import javax.persistence.Entity;
import java.util.Date;

@Entity
public class IncidenRegistry {
    private Date date;
    private String reason;
    private Double cuantificacion;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Double getCuantificacion() {
        return cuantificacion;
    }

    public void setCuantificacion(Double cuantificacion) {
        this.cuantificacion = cuantificacion;
    }
}
