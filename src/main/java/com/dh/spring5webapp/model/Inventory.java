package com.dh.spring5webapp.model;

import javax.persistence.Entity;

@Entity
public class Inventory {
    private Long quantity;
    private String status;

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
