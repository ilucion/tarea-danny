package com.dh.spring5webapp.model;

import javax.persistence.Entity;

@Entity
public class BuyOrder {
    private String unit;
    private Long quantity;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
