package com.dh.spring5webapp;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dh.spring5webapp.model.Category;

/**
 * Category Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Dec 11, 2017</pre>
 */
public class CategoryTest {

    private static final String RAW = "RAW";

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testTestUnitario() throws Exception {
        Category category = new Category();
        category.setName(RAW);
        assertEquals(RAW, category.getName());
        Assert.assertTrue(category.getName().equals(RAW));
    }

}
